var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var loggerMongo = require('mongo-morgan');
var abakoSch = require('./scheduler/abakoScheduler');
var cron = require('node-schedule');

console.log(abakoSch);
//cron.scheduleJob('* 30 2 * * 5', abakoSch.sendReport);
cron.scheduleJob('30 * * * * *', abakoSch);

var routes = require('./routes/index');
var clientes = require('./routes/clientes');
var compras = require('./routes/compras');
var productos = require('./routes/productos');
var sedes = require('./routes/sedes');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

//Mongoose space
var dbName = 'abako';
var localBaseUrl = 'mongodb://localhost:27017/'; //This is the local database URL
mongoose.connect(localBaseUrl + dbName);


app.use(loggerMongo(localBaseUrl + dbName, 'combined', {collection: 'logs'}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//avoiding CORS problems
app.use(function(req, res, next){
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST', 'PUT');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
  next();
});

//Using routes middleware
app.use('/', routes);
app.use('/clientes', clientes);
app.use('/compras', compras);
app.use('/productos', productos);
app.use('/sedes', sedes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;