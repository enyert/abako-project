/**
 * Model class for productos collection.
 * Created by enyert on 17/02/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productosSchema = new Schema({
  producto: { type: String, required: true},
  precio: { type: Number, required: true},
  descripcion: { type: String, required: true}
});

module.exports = mongoose.model('productos', productosSchema);

