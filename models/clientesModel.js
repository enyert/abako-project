/**
 * Model class for clientes collection.
 * Created by enyert on 17/02/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var clienteSchema = new Schema({
  documento: { type: Number, required: true, unique: true},
  nombres: { type: String, required: true},
  detalles: { type: String, required: false},
});

module.exports = mongoose.model('clientes', clienteSchema);
