/**
 * Model class for compras collection
 * Created by enyert on 17/02/16.
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var comprasSchema = new Schema({
  precio: { type: Number, required: true},
  descripcion: { type: String, required: true},
  fecha: { type: Date, required: true, default: Date.now },
  cliente: { type: Schema.Types.ObjectId, ref: 'clientes', required: true },
  sede: { type: Schema.Types.ObjectId, ref: 'sedes', required: false },
  productos: [{ type: Schema.Types.ObjectId, ref: 'productos', required: true }]
});

module.exports = mongoose.model('compras', comprasSchema);
