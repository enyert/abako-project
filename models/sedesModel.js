/**
 * Model class for sedes collection
 * Created by enyert on 17/02/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sedesSchema = new Schema({
  sede: { type: String, required: true, unique: true},
  direccion: { type: String, required: true}
});

module.exports = mongoose.model('sedes', sedesSchema);