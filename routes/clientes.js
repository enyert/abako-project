/**
 * Created by enyert on 17/02/16.
 */
var express = require('express');
var router = express.Router();
var Clientes = require('../models/clientesModel');


//Find a client by _id
router.get('/:id', function(req, res, next) {
  Clientes.findById(req.params.id, function(err, cliente) {
    if(err) return next(err);
    res.json(cliente);
  });
});

//Save a new cliente
router.post('/', function(req, res, next) {
  var clientes = new Clientes(req.body);

  clientes.save(function(err, cliente) {
    if(err) return next(err);
    res.json(cliente); //Return after a creation process is useful
  });
});

//Get all clientes
router.get('/', function(req, res, next) {
  Clientes.find(function (err, cliente) {
    if(err) return next(error);
    res.json(cliente);
  });
});

//Update a cliente by _id
router.put('/:id', function(req, res, next) {
  Clientes.findByIdAndUpdate(req.params.id, req.body, function(err) {
    if (err) return next(error);
    res.json(req.body);
  });
});

//delete a cliente
router.delete('/:id', function(req, res, next) {
  Clientes.findByIdAndRemove(req.params.id, req.body, function(err, cliente) {
    if (err) return next(error);
    res.json(cliente);
  });
});

module.exports = router;
