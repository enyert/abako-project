/**
 * Created by enyert on 17/02/16.
 */
var express = require('express');
var router = express.Router();
var Productos = require('../models/productosModel');

//Find a product by _id
router.get('/:id', function(req, res, next) {
  Productos.findById(req.params.id, function(err, producto) {
    if(err) return next(err);
    res.json(producto);
  });
});

//Save a new producto
router.post('/', function(req, res, next) {
  var producto = new Productos(req.body);

  producto.save(function(err, prods) {
    if(err) return next(err);
    res.json(prods); //Return after a creation process is useful
  });
});

//Get all productos
router.get('/', function(req, res, next) {
  Productos.find(function (err, prods) {
    if(err) return next(error);
    res.json(prods);
  });
});

//Update a producto by _id
router.put('/:id', function(req, res, next) {
  Productos.findByIdAndUpdate(req.params.id, req.body, function(err) {
    if (err) return next(error);
    res.json(req.body);
  });
});

//delete a producto
router.delete('/:id', function(req, res, next) {
  Productos.findByIdAndRemove(req.params.id, req.body, function(err, prods) {
    if (err) return next(error);
    res.json(prods);
  });
});

module.exports = router;