/**
 * Created by enyert on 17/02/16.
 */
var express = require('express');
var router = express.Router();
var Compras = require('../models/comprasModel');

//Find a client by _id
router.get('/:id', function(req, res, next) {
  Compras.findById(req.params.id, function(err, compra) {
    if(err) return next(err);
    res.json(compra);
  });
});

//Save a new compra
router.post('/', function(req, res, next) {
  var compras = new Compras(req.body);

  compras.save(function(err, compra) {
    if(err) return next(err);
    res.json(compra); //Return after a creation process is useful
  });
});

//Get all compra
router.get('/', function(req, res, next) {
  Compras.find(function (err, compras) {
    if(err) return next(error);
    res.json(compras);
  });
});

//Update a compra by _id
router.put('/:id', function(req, res, next) {
  Compras.findByIdAndUpdate(req.params.id, req.body, function(err) {
    if (err) return next(error);
    res.json(req.body);
  });
});

//delete a compra
router.delete('/:id', function(req, res, next) {
  Compras.findByIdAndRemove(req.params.id, req.body, function(err, compra) {
    if (err) return next(error);
    res.json(compra);
  });
});

module.exports = router;