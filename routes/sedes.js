/**
 * Created by enyert on 17/02/16.
 */
var express = require('express');
var router = express.Router();
var Sedes = require('../models/sedesModel');

//Find a sede by _id
router.get('/:id', function(req, res, next) {
  Sedes.findById(req.params.id, function(err, sede) {
    if(err) return next(err);
    res.json(sede);
  });
});

//Save a new sede
router.post('/', function(req, res, next) {
  var sedes = new Sedes(req.body);

  sedes.save(function(err, sedes) {
    if(err) return next(err);
    res.json(sedes); //Return after a creation process is useful
  });
});

//Get all sedes
router.get('/', function(req, res, next) {
  Sedes.find(function (err, sedes) {
    if(err) return next(error);
    res.json(sedes);
  });
});

//Update a sede by _id
router.put('/:id', function(req, res, next) {
  Sedes.findByIdAndUpdate(req.params.id, req.body, function(err) {
    if (err) return next(error);
    res.json(req.body);
  });
});

//delete a sede
router.delete('/:id', function(req, res, next) {
  Sedes.findByIdAndRemove(req.params.id, req.body, function(err, sede) {
    if (err) return next(error);
    res.json(sede);
  });
});

module.exports = router;