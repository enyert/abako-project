/**
 * Created by enyert on 17/02/16.
 */

var Compras = require('../models/comprasModel');


//Function to calculate compras in one week
function calculateCompras(date1, date2){
  Compras.count({"fecha": {"$gte": date1, "$lt": date2}}, function(err, quantity) {
    if (err) return -1;
    return quantity;
  });
};

//Function to calculate compras by minute in one week
function calculateComprasByMinute(date1, date2){
  var minutesPerWeek = parseFloat(7 * 24 * 60);
  var comprasThisWeek = calculateCompras(date1, date2);
  return comprasThisWeek / minutesPerWeek;
};

function sendReport(){
  date2 = new Date();
  date1 = new Date();
  date1.setDate(date1.getDate() - 7);
  var reporte = {
    totalCompras: calculateCompras(date1, date2),
    comprasPorMinuto: calculateComprasByMinute(date1, date2)
  };

  console.log(reporte);
};

module.exports = sendReport;